db.users.insertMany([
		{
			firstName: "Russel",
			lastName: "Allen",
			email: "ra@gmail.com",
			password: "russel123",
			isAdmin: false
			
		},
		{
			firstName: "Steve",
			lastName: "Augeri",
			email: "sa@gmail.com",
			password: "steve123",
			isAdmin: false
		},
		{
			firstName: "Trey",
			lastName: "Anastasio",
			email: "ta@gmail.com",
			password: "trey123",
			isAdmin: false
		},
		{
			firstName: "Nate",
			lastName: "Albert",
			email: "na@gmail.com",
			password: "nate123",
			isAdmin: false
		},
		{
			firstName: "Eric",
			lastName: "Adams",
			email: "ea@gmail.com",
			password: "eric123",
			isAdmin: false
		},
	])

db.users.find()
db.users.updateOne({}, {$set: {isAdmin: "true"}})


db.courses.insertMany([
		{
			"name": "HTML",
			"price": 1500,
			"isActive" : false
			
		},
		{
			"name": "CSS",
			"price": 2500,
			"isActive" : false
		},
		{
			"name": "Bootstrap",
			"price": 3500,
			"isActive" : false
		},

	])

db.courses.find()
db.courses.updateOne({name: "CSS"}, {$set: {isActive: "true"}})

db.courses.deleteMany({})


